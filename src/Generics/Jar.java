package Generics;

import java.util.ArrayDeque;
import java.util.Deque;

public class Jar<T> {

    Deque<T> elements;

    public Jar() {
        this.elements = new ArrayDeque<>();
    }


    public void add(T content) {
        this.elements.push(content);
    }

    public T remove() {
        return this.elements.pop();
    }
}
