package MultidimentionalArrays;

import java.util.Arrays;
import java.util.Scanner;

public class multiArrays {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] input = scanner.nextLine().split(", ");
        int size = Integer.parseInt(input[0]);
        String pattern = input[1];
        int[][] matrix = new int[size][size];

        if (pattern.equals("A")) {
            paramA(matrix);
        } else {
            paramB(matrix);
        }
        printMatrix(matrix);
    }

    private static void printMatrix(int[][] matrix) {

        for (int row = 0; row < matrix.length; row++) {
            for (int j = 0; j < matrix[row].length; j++) {
                System.out.print(matrix[row][j] + " ");
            }
            System.out.println();
        }
    }


    private static void paramA(int[][] matrix) {
        int counter = 1;
        for (int column = 0; column < matrix.length; column++) {
            for (int row = 0; row < matrix.length; row++) {
                matrix[row][column] = counter++;
            }
        }
    }


    private static void paramB(int[][] matrix) {
        int counter = 1;
        for (int col = 0; col < matrix.length; col++) {
            if (col % 2 == 0) {
                for (int row = 0; row < matrix.length; row++) {
                    matrix[row][col] = counter++;
                }

            } else {
                for (int row = matrix.length-1; row >= 0; row--) {
                    matrix[row][col] = counter++;
                }
            }
        }
    }
}