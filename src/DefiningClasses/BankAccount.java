package DefiningClasses;

public class BankAccount {

    private final static double DEFAULT_INTEREST_RATE = 0.2;

    private static double interestRate = DEFAULT_INTEREST_RATE;
    private static int bankAccountCount;
    private static int id;

    private double balance;


    public BankAccount() {
        this.id = ++bankAccountCount;
    }

    //setters and getters
    public static void setInterestRate(double interestRate) {
        BankAccount.interestRate = interestRate;
    }

    public static double getInterestRate(int years) {
        return interestRate;
    }

    public static void create() {
        new BankAccount();
        System.out.printf("Account ID" + BankAccount.getId());
    }

    void deposit(double amount) {
        this.balance += amount;
    }


    public static int getId() {
        return BankAccount.id;
    }
}
