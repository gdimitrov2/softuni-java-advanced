package santaFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        String[] materials = scanner.nextLine().split(" ");
        String[] magicLevel = scanner.nextLine().split(" ");
        ArrayList<Integer> materialsList = new ArrayList<>();
        ArrayList<Integer> magicLevelList = new ArrayList<>();

        for (String material : materials) {
            materialsList.add(Integer.parseInt(material));
        }

        for (String magicElement : magicLevel) {
            magicLevelList.add(Integer.parseInt(magicElement));
        }
        int lastMaterial = materialsList.get(materialsList.size() - 1);
        int firstMagic = magicLevelList.get(0);
        int totalMagic = lastMaterial * firstMagic;

        HashMap<String, Integer> presents = new HashMap<>();

        int dolls = 0;
        int trains = 0;
        int bears = 0;
        int bicycles = 0;


        while (magicLevelList.size() > 0 && materialsList.size() > 0) {
            lastMaterial = materialsList.get(materialsList.size() - 1);
            firstMagic = magicLevelList.get(0);
            totalMagic = lastMaterial * firstMagic;
            if (firstMagic == 0) {
                magicLevelList.remove(0);

            } else if (lastMaterial == 0) {
                materialsList.remove(lastMaterial);
            } else if (totalMagic < 0) {
                int addValue = lastMaterial + firstMagic;
                removeFirstLast(materialsList, magicLevelList);
                materialsList.add(addValue);
            } else if (totalMagic == 150) {
                removeFirstLast(materialsList, magicLevelList);

                dolls++;
            } else if (totalMagic == 250) {
                removeFirstLast(materialsList, magicLevelList);

                trains++;
            } else if (totalMagic == 300) {
                removeFirstLast(materialsList, magicLevelList);
                bears++;
            } else if (totalMagic == 400) {
                removeFirstLast(materialsList, magicLevelList);
                bicycles++;
            } else {
                magicLevelList.remove(0);
                materialsList.set(materialsList.size() - 1, lastMaterial + 15);
            }
        }

        presents.put("Dolls", dolls);
        presents.put("Wooden train", trains);
        presents.put("Teddy bear", bears);
        presents.put("Bicycle", bicycles);

        boolean done = false;

        if ((dolls > 0 && trains > 0) || (bears > 0 && bicycles > 0)) {
            done = true;
        }


        if (done == true) {
            System.out.println("The presents are crafted! Merry Christmas!");
            System.out.println("Materials left: " + materialsList.toString());
            System.out.println("Magic left: " + magicLevelList.toString());

        }

    }

    private static void removeFirstLast(ArrayList<Integer> materialsList, ArrayList<Integer> magicLevelList) {
        magicLevelList.remove(0);
        materialsList.remove(materialsList.size() - 1);
    }
}
